import snowflake.connector
import json


def get_ctx():
    return snowflake.connector.connect(
        user=<USER>,
        password=<PW>,
        account=<ACCOUNT>,
        warehouse=<WH>,
        database="DEMO",
        schema="ADMIN")


def get_resultset(query):
    results = []
    try:
        with get_ctx() as ctx:
            cur = ctx.cursor()
            rs = cur.execute(query)
            desc = rs.description
            results = [{desc[i][0]: field for i, field in enumerate(row)} for row in rs]
    except Exception as e:
        print(e)
        
    return results


def get_explain(query_id):   
    explain_query = f"SELECT SYSTEM$EXPLAIN_PLAN_JSON('{query_id}') as plan_json"
    return json.loads(get_resultset(explain_query)[0]["PLAN_JSON"])


def scanned_tables(explain):
    return {row["objects"][0] for row in explain["Operations"][0] if row["operation"] == "TableScan"}


query_ids = [{"QUERY_ID": "01960888-02be-1a4a-0000-03613f7a777e"},
             {"QUERY_ID": "01960b1d-02b0-35cd-0000-03613f813276"}]
for qid in query_ids:
    print(f"{qid}:")
    expl = get_explain(qid["QUERY_ID"])
    print("\t{}\n".format(scanned_tables(expl)))
