from setuptools import setup


setup(
    name='snowflake_scripts',
    versioning='devcommit',
    author='GC Data Group Inc.',
    author_email='gcdatagroup@gcdatagroup.com',
    description='A set of admin scripts for the Snowflake datawarehouse',
    keywords='snowflake',
    url='',
    python_requires='>=3.4',
    scripts=['bin/populate_reclustering_info'],
    install_requires=['docopt',
                      'PyYAML',
                      'snowflake-connector-python'],
    setup_requires=['setupmeta']
)