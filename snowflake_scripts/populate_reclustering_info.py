
import logging
from multiprocessing.dummy import Pool
import queue
import time

import snowflake.connector


_logger = logging.getLogger(__name__)
ctx_queue = queue.Queue()


def initialize_ctx(admin_user, password, account, admin_db, admin_wh, admin_schema, threads):
    """
    Populate the connection pool with connections

    Parameters
    ----------
    admin_user: string
        The user to run all the statements
    account: string
        The Snowflake account for the connections
    admin_db:
        The database to use for all admin tables
    admin_wh:
        The default warehouse to use for the connection
    threads: int, default: 5
        The number of connections in the connection pool, also the size of the threadpool used when
        acquiring the connections
    """
    with Pool(threads) as thread_pool:
        thread_pool.starmap(connect_ctx, [(i, admin_user, password, account, admin_db, admin_wh, admin_schema) for i in range(threads)])


def connect_ctx(i, admin_user, password, account, admin_db, admin_wh, admin_schema):
    """
    Acquire a snowflake connection ctx

    Parameters
    ----------
    i: int, default: 5
        for informational purposes while threads are acquiring the connections
    admin_user: string
        The user to run all the statements
    account: string
        The Snowflake account for the connections
    admin_db:
        The database to use for all admin tables
    admin_wh:
        The default warehouse to use for the connection
    """
    _logger.info("ctx %s: initializing ctx" % i)
    try:
        ctx = snowflake.connector.connect(user=admin_user,
                                          password=password,
                                          account=account,
                                          schema=admin_schema,
                                          warehouse=admin_wh,
                                          database=admin_db,
                                          autocommit=True)
        ctx_queue.put(ctx)
    except Exception as e:
        _logger.error(e)

    _logger.info("ctx %s: done" % i)


def populate_clusterting_hist_table(tbl, warehouse, data_db, admin_schema):
    """
    Parameters
    ----------
    tbl: tuple
        consists of a tuple strings, first element is the schemaname, second_element is tablename
    warehouse: string
        the warehouse to use to execute the statements
    data_db: string
        the database where the data tables reside
    admin_schema:
        the schema where the partition_depth_history table is stored

    Returns
    -------
    boolean
        whether or not all the statements executed
    """
    ctx = ctx_queue.get(timeout=30)
    schemaname, tblname = tbl

    _logger.info(tbl[0] + "." + tbl[1])
    stmts = list()

    stmts.append("""USE WAREHOUSE {}""".format(warehouse))
    stmts.append("""SELECT SYSTEM$CLUSTERING_INFORMATION('{data_db}.{schema}.{table}')""".format(data_db=data_db,
                                                                                                 schema=schemaname,
                                                                                                 table=tblname))

    stmts.append("""INSERT INTO {admin_schema}.partition_depth_history
SELECT CURRENT_TIMESTAMP as ins_ts, '{data_db}', '{schema}' as schemaname, '{table}' as tablename,key as bucket,value::INT as num_partitions
FROM (
SELECT  key, value                                                                              
FROM TABLE(RESULT_SCAN())t1 ,
  LATERAL FLATTEN(input => parse_json("SYSTEM$CLUSTERING_INFORMATION('{data_db}.{schema}.{table}')"):partition_depth_histogram)
) t1
""".format(data_db=data_db,
           admin_schema=admin_schema,
           schema=schemaname,
           table=tblname))
    success = True
    try:
        cur = ctx.cursor()
        for stmt in stmts:
            _logger.debug(stmt)
            cur.execute(stmt)
        cur.close()
    except Exception as e:
        _logger.error(e)
        success = False

    ctx_queue.put(ctx)
    return success


def get_tables(data_db="DSE", schema_list=None):
    """
    Provide the logic for what tables to track history on, this implementation shows getting all tables
    in a list of schemas that have clustering keys

    Parameters
    ----------
    data_db: string
        the database where the data tables reside
    schema_list: string, list
        the list of schemas to include in the table search

    Returns
    -------
    list
        a list of schemaname, tablename tuples
    """
    ctx = ctx_queue.get(timeout=10)
    if isinstance(schema_list, str):
        schema_list = schema_list.split(",")
    schema_clause = ("" if schema_list is None
                     else "and table_schema in ({})".format(",".join(["'{}'".format(schema)
                                                                  for schema in schema_list])))

    clustered_tbls = """SELECT table_schema, table_name, row_count
FROM {data_db}.INFORMATION_SCHEMA.tables
WHERE clustering_key is not null
    {schema_clause} 
ORDER BY 1,2""".format(data_db=data_db,
                       schema_clause=schema_clause)
 
    with ctx.cursor() as cur:
        rs = cur.execute(clustered_tbls)
        tables = [(row[0], row[1]) for row in rs]
    
    ctx_queue.put(ctx)
    return tables
    

def cleanup(timeout=300):
    """
    Get all the snowflake connections off the queue and close them

    Parameters
    ----------
    timeout: int
        The amount of time in seconds to wait to acquire the connections
    """
    _logger.info("cleaning up connections")
    cleanup_start = int(time.time())
    while not ctx_queue.empty() and int(time.time()) - cleanup_start < timeout:
        try:
            ctx_queue.get(timeout=10).close()
        except queue.Empty:
            logging.warning("Unable to get item within timeout, looping back")


def load_args_from_config(filename):
    import yaml
    with open(filename, 'r') as stream:
        config_file_args=dict()
        try:
            config_file_args = yaml.load(stream)
        except yaml.YAMLError as exc:
            _logger.error(exc)
        return config_file_args


def run_populate_reclustering_info(admin_user, password, account, admin_db,
                                   admin_wh, data_db, admin_schema, schema_list, threads):
    start = int(time.time())

    thread_pool = Pool(threads)
    if password is None:
        # if password is not passed-in through config file, add a password
        # method here.
        # password = some_secure_service_call_for_password()
        raise RuntimeError("No password found.")

    initialize_ctx(admin_user, password, account, admin_db, admin_wh, admin_schema, threads)

    logging.getLogger(__name__).setLevel(logging.DEBUG)
    tables = get_tables(schema_list=schema_list)
    _logger.debug(tables)
    _logger.debug(thread_pool.starmap(populate_clusterting_hist_table,
                                      [(table, admin_wh, data_db, admin_schema) for table in tables]))
    end = int(time.time())
    cleanup()
    _logger.info("Took {} secs".format(end - start))